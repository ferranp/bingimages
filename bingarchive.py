#!/usr/bin/python


import urllib
import re
import os
import simplejson
import datetime

today = datetime.datetime.today()

image_day = datetime.datetime(2009,6,1)
one_day = datetime.timedelta(days=1)
urlbase = "http://www.istartedsomething.com/bingimages/"

while image_day <= today:
    if not os.path.isdir(str(image_day.year)):
       os.mkdir(str(image_day.year))
    image_file_name = os.path.join('.',str(image_day.year),image_day.strftime('%Y%m%d')+'.jpg')
    if os.path.exists(image_file_name):
       image_day = image_day + one_day
       continue
    jsonurl = urlbase + "getimage.php?id=%s-us" % image_day.strftime('%Y%m%d') 
    data = simplejson.loads(urllib.urlopen(jsonurl).read())
    if not data:
       image_day = image_day + one_day
       continue
    imageurl = urlbase + "cache/" + data['imageurl']
    print image_day.strftime('%Y%m%d') , imageurl
    image = urllib.urlopen(imageurl)
    imagefile = open(image_file_name,'w')
    imagefile.write(image.read())
    imagefile.close()
    image_day = image_day + one_day


