#!/usr/bin/python


import urllib
import re
import os

page = urllib.urlopen("http://www.bing.com").read()
p = re.search(";g_img={url:'(.*?)'",page)
imagename = p.group(1).replace('\/','/')
imageurl = "http://www.bing.com" + imagename

image = urllib.urlopen(imageurl)
imagefile = open(os.path.basename(imagename),'w')
imagefile.write(image.read())
imagefile.close()

